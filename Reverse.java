import java.util.*;

public class Reverse {
	public static void main(String[] args) {
		GenetricStack<String> stack = new GenetricStack<>();
		Scanner in = new Scanner(System.in);
		System.out.println("Enter 5 strings: ");
		Scanner input = new Scanner(System.in);
        System.out.print("Enter 5 strings: ");
        for (int i = 0; i < 5; i++) 
        	stack.push(input.next());
        while(!stack.isEmpty()) {
            System.out.println(stack.pop());
        }

	}
}

	class GenetricStack<E> extends ArrayList<E> {

	public E peek() {
        return get(size() - 1);
    }

    public void push(E o) {
        add(o);
    }

    public E pop() {
        E o = get(size() - 1);
        remove(size() - 1);
        return o;
    }

    public boolean isEmpty() {
        return super.isEmpty();
    }

    @Override
    public String toString() {
        return "stack: " + toString();
    }
}

